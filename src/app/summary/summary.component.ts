import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SummaryService } from './summary.service';
import { Observable, interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  periodId: string;
  summary: any;

  constructor(
    private summaryService: SummaryService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.periodId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.summaryService.getPeriodSummary(this.periodId).subscribe(json => {
      this.summary = json;
    });
  }

}
