import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SummaryService {

  constructor(private httpClient: HttpClient) { }

  getPeriodSummary(periodId: string) {
    return this.httpClient.get('http://localhost:8080/api/summary/period/' + periodId);
  }
}
