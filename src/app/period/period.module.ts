import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeriodSelectComponent } from './period-select/period-select.component';

@NgModule({
  declarations: [PeriodSelectComponent],
  imports: [
    CommonModule
  ]
})
export class PeriodModule { }
