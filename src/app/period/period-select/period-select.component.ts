import { Component, OnInit } from '@angular/core';
import { PeriodService } from '../period.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-period-select',
  templateUrl: './period-select.component.html',
  styleUrls: ['./period-select.component.css']
})
export class PeriodSelectComponent implements OnInit {

periods: any;

  constructor(
    private periodService: PeriodService,
    private router: Router
  ) { }

  ngOnInit() {
    this.periodService.getPeriods().subscribe(json => {
      this.periods = json;
    });
  }

  changePeriod(periodId: String) {
    if(periodId != '0') {
      this.router.navigate(['/summary/' + periodId]);
    }
  }

}
