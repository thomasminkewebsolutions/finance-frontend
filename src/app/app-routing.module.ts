import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component'
import { SummaryComponent } from './summary/summary.component'
import { ExpenseAddComponent } from './expense-add/expense-add.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent
  },
  {
    path: 'summary/:id',
    component: SummaryComponent
  },
  {
      path: 'expense/add',
      component: ExpenseAddComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
